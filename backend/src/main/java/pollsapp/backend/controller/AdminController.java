package pollsapp.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pollsapp.backend.exception.BadRequestException;
import pollsapp.backend.payload.request.SignInRequest;
import pollsapp.backend.payload.request.SignUpRequest;
import pollsapp.backend.payload.response.ApiResponse;
import pollsapp.backend.payload.response.AuthenticationResponse;
import pollsapp.backend.security.SecurityConstants;
import pollsapp.backend.service.AdminService;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/auth")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @PostMapping(value = "/signup")
    public ApiResponse signup(@Valid @RequestBody SignUpRequest signUpRequest) throws BadRequestException {
        return adminService.signup(signUpRequest);
    }

    @PostMapping(value = "/signin")
    public ResponseEntity signin(@Valid @RequestBody SignInRequest signInRequest) {
        AuthenticationResponse response = adminService.signin(signInRequest);
        HttpHeaders headers = new HttpHeaders();
        headers.add(SecurityConstants.HEADER_STRING, response.getToken());
        return new ResponseEntity(response, headers, HttpStatus.OK);
    }
}

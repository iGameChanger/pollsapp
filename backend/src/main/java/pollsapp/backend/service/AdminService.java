package pollsapp.backend.service;

import pollsapp.backend.exception.BadRequestException;
import pollsapp.backend.payload.request.SignInRequest;
import pollsapp.backend.payload.request.SignUpRequest;
import pollsapp.backend.payload.response.ApiResponse;
import pollsapp.backend.payload.response.AuthenticationResponse;

public interface AdminService {

    ApiResponse signup(SignUpRequest signUpRequest) throws BadRequestException;

    AuthenticationResponse signin(SignInRequest signInRequest);


}

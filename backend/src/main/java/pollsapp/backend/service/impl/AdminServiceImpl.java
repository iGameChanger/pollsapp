package pollsapp.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pollsapp.backend.entity.AppRole;
import pollsapp.backend.entity.AppUser;
import pollsapp.backend.enums.RoleEnum;
import pollsapp.backend.exception.AppException;
import pollsapp.backend.exception.BadRequestException;
import pollsapp.backend.payload.request.SignInRequest;
import pollsapp.backend.payload.request.SignUpRequest;
import pollsapp.backend.payload.response.ApiResponse;
import pollsapp.backend.payload.response.AuthenticationResponse;
import pollsapp.backend.repository.AppRoleRepository;
import pollsapp.backend.repository.AppUserRepository;
import pollsapp.backend.security.JwtTokenProvider;
import pollsapp.backend.service.AdminService;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class AdminServiceImpl implements AdminService {

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    private AppRoleRepository roleRepository;
    @Autowired
    private AppUserRepository userRepository;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Override
    public ApiResponse signup(SignUpRequest signUpRequest) throws BadRequestException {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            throw new BadRequestException("Username already taken");
        }
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            throw new BadRequestException("Email already registered");
        }

        AppRole role = roleRepository.findByRoleName(RoleEnum.ROLE_ADMIN).orElse(null);
        AppUser user = new AppUser(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));
        Set<AppRole> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);
        AppUser savedUser = userRepository.save(user);
        if (savedUser == null) {
            throw new AppException("Some error occured");
        }
        return new ApiResponse(true, "User registered successfully");
    }

    @Override
    public AuthenticationResponse signin(SignInRequest signInRequest) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(signInRequest.getUsername(), signInRequest.getPassword());
        Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtTokenProvider.generateToken(authentication);
        return new AuthenticationResponse(token);
    }


}

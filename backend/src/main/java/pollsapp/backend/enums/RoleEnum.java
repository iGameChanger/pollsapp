package pollsapp.backend.enums;

public enum RoleEnum {

    ROLE_ADMIN, ROLE_USER
}

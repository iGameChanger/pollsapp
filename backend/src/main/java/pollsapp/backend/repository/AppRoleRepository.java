package pollsapp.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pollsapp.backend.entity.AppRole;
import pollsapp.backend.enums.RoleEnum;

import java.util.Optional;

@Repository
public interface AppRoleRepository extends JpaRepository<AppRole, Long> {

    Optional<AppRole> findByRoleName(RoleEnum roleName);
}

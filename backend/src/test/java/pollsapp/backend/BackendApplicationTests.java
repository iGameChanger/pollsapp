package pollsapp.backend;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import pollsapp.backend.entity.AppRole;
import pollsapp.backend.entity.AppUser;
import pollsapp.backend.enums.RoleEnum;
import pollsapp.backend.repository.AppRoleRepository;
import pollsapp.backend.repository.AppUserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BackendApplicationTests {

    @Autowired
    private AppRoleRepository roleRepository;

    @Autowired
    private AppUserRepository userRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Test
    public void contextLoads() {
    }

//    @Test
    public void createDefaultRoles() {
        AppRole roleAdmin = new AppRole(RoleEnum.ROLE_ADMIN);
        roleRepository.save(roleAdmin);

        AppRole roleUser = new AppRole(RoleEnum.ROLE_USER);
        roleRepository.save(roleUser);
    }

//    @Test
    public void createDefaultUser() {
        AppUser user = new AppUser("admin", "admin", "admin@gmail.com", encoder.encode("admin"));
        AppRole role = roleRepository.findByRoleName(RoleEnum.ROLE_ADMIN).orElse(null);
        user.getRoles().add(role);
        userRepository.save(user);
    }

}
